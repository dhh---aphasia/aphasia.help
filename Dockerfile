## stage-0 as ‘node’
FROM node:11 as node
WORKDIR /webapp
# Storing node modules on a separate layer will prevent unnecessary npm installs at each build
COPY website/package.json package.json
RUN npm install
# Build the angular app in production mode and store the artifacts in dist folder
COPY website/ .
RUN npm run ng build --prod

## stage-1 as 'maven'
FROM maven:3-jdk-11 as maven
WORKDIR /app
# cache dependencies separately
# only runs if pom.xml changes
COPY backend/pom.xml .
RUN mvn verify clean --fail-never
# build from source
COPY backend/src/ ./src/
COPY --from=node /webapp/dist/DHH-Aphasia/ ./src/main/resources/static/
RUN mvn package

# stage-2 for execution
FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=maven /app/target/backend-*.jar /app/AphasiaHelp.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","AphasiaHelp.jar"]
EXPOSE 8080
