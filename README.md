# aphasia.help

## Check out
```
git clone --recursive https://gitlab.com/dhh---aphasia/aphasia.help.git
```
## Prepare
You will need both OAUTH credentials for Google and Orcid. Set them in docker-compose.dev.yaml, or in docker-compose.prod.yaml which is excluded from git.

## Run
Please note: The first build will take some while since dependencies need to be resolved both for the Node website via npm and the Java backend via maven. Unless you change package.json or pom.xml all rebuilds should be below 10s.

```
docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up -d --build --force-recreate
```

Website runs on http://localhost:8080